# Strogonof de Frango
### Ingredientes
 - 500 gramas de peito de frango cortado em cubos
 - 2 colheres(sopa) de manteiga
 - 1 cebola ralada
 - 1 xícara (chá) de creme de leite fresco
 - ½ xícara (chá) de ketchup
 - 3 colheres(sopa) de molho inglês
 - 1 colher(sopa) de mostarda
 - 1 xícara (chá) de champignon fatiado
 - ½ xícara (chá) de conhaque
 - Sal, pimenta-do-reino, farinha de trigo e cheiro-verde a gosto

#### Para Acompanhar:
 - Arroz branco e batata palha a gosto

### Modo de Preparo
Em uma frigideira coloque um pouco de azeite, refogue o frango com manteiga, deixe pegar no fundo da panela para dar cor, depois coloque o conhaque e flambe.
Acrescente a cebola e refogue bem até soltar as crostinhas do fundo da panela, inclua o restante dos ingredientes, misture e deixe cozinhar até apurar e encorpar. Sirva acompanhado de arroz branco e batata palha.

#### Rendimento: 4 porções

Tempo de preparo: 30M
